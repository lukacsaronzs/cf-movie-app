<!---Form processing script starts here--->

<cfif structkeyExists(form,'fld_newUserSubmit')>
	<!---Server side data validation--->
	
	<cfset aErrorMessages = application.userService.validateUser(fld_userName,fld_userEmail,fld_userPassword)/>
	<cfif ArrayIsEmpty(aErrorMessages)>		
		<cfif arrayisempty(aErrorMessages)>
			<!---If no errors add the new user--->

		 	<cfset userAdded = application.userService.addUser(fld_userName,fld_userEmail,fld_userPassword) />
		</cfif>
	</cfif>
	

</cfif>


<cfinclude template="./Includes/head.cfm" /> 


	<h2>Please enter the following information!</h2>
			<cfform id="frm_newUser">
				
				<fieldset>
					<legend>Register</legend>
					<cfif isDefined('aErrorMessages') AND NOT ArrayIsEmpty(aErrorMessages)>	
					<cfoutput >
						<cfloop array = "#aErrorMessages#" index = "message">
							<p class="errorMessage">#message#</p>
						</cfloop>
					</cfoutput>
					
				</cfif>
					<dl>
						<dt><label>Name</label></dt>
						<dd><cfinput type="text" name="fld_userName" id="fld_userName" required="true" message = "Please enter your name!"/></dd>

						<dt><label>E-mail Address</label></dt>
						<dd><cfinput  type="text" name="fld_userEmail" id="fld_userEmail"  required="true" validate="eMail" message="Please enter a valid email address!"/></dd>
						

						<dt><label>Password</label></dt>
						<dd><cfinput  type="password" name="fld_userPassword" id="fld_userPassword"  required="true" message="Please choose a password!"/></dd>

					</dl>
					<input type="submit" name="fld_newUserSubmit" id="fld_newUserSubmit" value="Register" />
					<cfif isdefined('userAdded') AND userAdded>
						<cfoutput >
							<p>Registration complete! You can log in now <a href="/testMovieApp/index.cfm">here</a></p>
					</cfoutput>
					<cfelseif isdefined('userAdded') AND NOT userAdded>
							<p class=errorMessage>Something went wrong. Try again later.</p>

					</cfif>
				</fieldset>
			</cfform>




</body>
</html>





