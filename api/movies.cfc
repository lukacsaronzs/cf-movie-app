component rest="true" restpath="all" {

	remote any function getAllMovies() httpMethod="POST" {
		movies = application.movieService.getAllMovies();
		res["recordsTotal"] = arrayLen(movies);
		rest = [];
		if (!isDefined(form.length)){
			form.length = arrayLen(movies);
		}
		if (!isDefined(form.start)){
			form.start = 0;
		}
		for(i = 1; i <= form.length and form.start + i <= arrayLen(movies); i++){
			movie = movies[form.start + i];
			m = {};
            m["price"] = movie.getprice();
            m[ "created_at"] =  movie.getcreated_at();
            m[ "created_by"] =  movie.getcreated_by();
            m[ "description"] =  movie.getdescription();
            m[ "movie_id"] = movie.getmovie_id();
            m[ "title"] =  movie.gettitle();
            m[ "release_year"] =  movie.getrelease_year();
            m[ "updated_at"] =  movie.getupdated_at();
            m[ "updated_by"] =  movie.getupdated_by();
			rest.append(m)
			}
		res["draw"]= form.draw;
		res["recordsFiltered"] = res["recordsTotal"];
		res["data"] = rest
		return res;
	}

}
