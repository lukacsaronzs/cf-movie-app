component  output="false"
{
	public any function getUserByCredentials(required string userEmail, required string userPassword){
		return EntityLoad('User', {email=userEmail, password = hash(userPassword, "SHA-256", "UTF-8")}, true);

	}
	
	public any function getUserByID(required numeric userID ){
		return EntityLoad('User', userID, true);

	}
	
	public  boolean function addUser(required string userName,required string userEmail,required string userPassword)
	 output="false"
	{
		try {
			newUser = EntityNew("User");
			newUser.setname(userName);
			newUser.setemail(userEmail);
			newUser.setpassword(hash(userPassword, "SHA-256", "UTF-8"));
			Entitysave(newUser);
			return true;
			
			} catch(Exception ex) {
				return false; 
			}
	}

	public array function validateUser(required string userName,required string userEmail,required string userPassword)
	output="false"
	{
		var aErrorMessages = [];
		// Validate name
		if (len(userName) < 5 OR len(userName) > 100) {
			arrayAppend(aErrorMessages,"Please provide a name between 5 and 100 characters!");
		}
		// Validate email address

		if (userEmail == '' OR !isValid("eMail", userEmail) ) {
			arrayAppend(aErrorMessages,"Please provide a valid email address!");
		}
	    // Validate email address unicity
	    var userWithSameEmailAddress = EntityLoad('User', {email=userEmail}, false);
		if (arraylen(userWithSameEmailAddress) != 0){
			arrayAppend(aErrorMessages,"Email address already in use!");
		}
		// Validate password

		if (len(userPassword) < 5 OR len(userPassword) > 100) {
			arrayAppend(aErrorMessages,"Please enter a password between 5 and 100 characters!");
		}
		return aErrorMessages;
		}

}