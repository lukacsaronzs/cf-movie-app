component output="false" {
	// Authentication component
	// doLogin() method

	public boolean function doLogin(required string userEmail, required string userPassword) 
	output=false 
	{
		var isLoggedIn = false;
		// Get user from database
		var user = application.userService.getUserByCredentials(userEmail, userPassword);
		// Check if querry returns one record
		if ( ! isnull(user) ) {
			// Log user in
			cflogin(  ) {
 				cfloginuser(name=user.getname(), password=user.getpassword(), roles="user");
			}
			// Save user in session
			session.loggedInUser = user;
			isLoggedIn = true;
		}
		return isLoggedIn;
	}
	// doLogout() method

	public void function doLogout() output=false {
		structdelete(session, 'loggedInUser');
		cflogout(  );
	}

}
