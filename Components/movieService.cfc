component  output="false"
{
	
	public array function getAllMovies() output="false"   
	{	
		var movies = EntityLoad('Movie');
		return movies;
	}
	
	public struct function getMovieByID(required numeric movieID)
	output="false"
	{
		return EntityLoad('Movie', movieID, true);;
	}

	public array function validateMovie(required string title,required string description,required string release_year,required string price)
	 output="false"
	{
		var aErrorMessages = [];
		// Validate title
		if (len(title) < 5 OR len(title) > 60) {
			arrayAppend(aErrorMessages,"Please provide a title between 5 and 60 characters!");
		}
		// Validate description

		if (len(description) < 5 OR len(description) > 60) {
			arrayAppend(aErrorMessages,"Please provide a description between 5 and 500 characters!");
		}

		//validate release year
		if (release_year < 1900 OR release_year > year(now())) {
			arrayAppend(aErrorMessages,"Please enter a valid release year!");
		}
		//validate price
		if (price < 0) {
			arrayAppend(aErrorMessages,"Please enter a positive sum!");
		}
		//validate price and release year types
		if (not isValid("integer", release_year)){
						arrayAppend(aErrorMessages,"Release year must be a number!");
		}
		if (not isValid("float", price)){
						arrayAppend(aErrorMessages,"Price must be a number!");
		}
		
		
		return aErrorMessages;
	}
		



	public boolean function deleteMovie(required numeric movieID)
	output="false"
	{
		var movie = EntityLoad("Movie",movieID, true);
		try{
			EntityDelete(movie);
			return true;

		}catch(Exception exc){
			return false;
		}
		
	}


	public boolean function addMovie(required string title,required string description,required string release_year,required string price, required numeric created_by)
	 output="false"
	{
			try {
			var movie = Entitynew("Movie");
			movie.settitle(title);
			movie.setdescription(description);
			movie.setrelease_year(LSParseNumber(release_year));
			movie.setprice(LSParseNumber(price));
			
			var user = EntityLoadByPK( 'User', created_by );

			// Only then can we successfully update the template's foreign key property
			movie.setcreated_by(user);
			movie.setcreated_at(now());
			Entitysave(movie);
			return true;
			
			} catch(Exception ex) {
				return false; 
			}
			
	}
	
	public boolean function updateMovie(required numeric movieID, required string title,required string description,required string release_year,required string price, required numeric updated_by)
	 output="false"
	{
			try{
				var movie = EntityLoad("Movie",movieID, true);
				movie.settitle(title);
				movie.setdescription(description);
				movie.setrelease_year(LSParseNumber(release_year));
				movie.setprice(LSParseNumber(price));
				
				
				var user = EntityLoadByPK( 'User', updated_by );
				movie.setupdated_by(user);
				
				movie.setupdated_at(now());
				Entitysave(movie);
				return true;
			
			} catch(Exception ex) {
				return false; 
			}
	}

}