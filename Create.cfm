<cfinclude template="./Includes/accesscontrol.cfm" /> 


<cfif structKeyExists(form,'fld_submitEdit')>
		<!---Proceed to edit procedure--->
		<cfset aErrorMEssages = application.movieService.validateMovie(fld_movieTitle,fld_movieDescription,fld_movieReleaseYear, fld_moviePrice)/>
		<cfif arraylen(aErrorMEssages) EQ 0>

			<cfset executed = application.movieService.addMovie(fld_movieTitle,fld_movieDescription,fld_movieReleaseYear,fld_moviePrice,fld_userId)>
			<cfif executed>
				<cflocation url="/testMovieApp/index.cfm" />
			</cfif>
		</cfif>
</cfif>



<cfinclude template="./Includes/head.cfm" /> 



<cfinclude template="./Includes/loginForm.cfm" /> 


	<cfform id="frmConnexion" preservedata="true">
	<fieldset>
    <legend>Edit movie</legend>
					<cfif isDefined('aErrorMessages') AND NOT ArrayIsEmpty(aErrorMessages)>	
					<cfoutput >
						<cfloop array = "#aErrorMessages#" index = "message">
							<p class="errorMessage">#message#</p>
						</cfloop>
					</cfoutput>
				</cfif>
		<dl>
			<dd><cfinput type="hidden" name="fld_userId" id="fld_userId" required="true" value = #session.loggedInUser.getuser_id()#/></dd>

        	<dt><label for="fld_movieTitle">Title</label></dt>
            <dd><cfinput type="text" name="fld_movieTitle" id="fld_movieTitle" required="true" /></dd>
            
            <dt><label for="fld_movieDescription">Description</label></dt>
            <dd><cfinput type="text" name="fld_movieDescription" id="fld_movieDescription" required="true"  /></dd>
            
            <dt><label for="fld_movieReleaseYear">Release year</label></dt>
            <dd><cfinput type="text" name="fld_movieReleaseYear" id="fld_movieReleaseYear" required="true"/></dd>
            
            <dt><label for="fld_moviePrice">Price</label></dt>
            <dd><cfinput type="text" name="fld_moviePrice" id="fld_moviePrice" required="true"  /></dd>
            
    		
        </dl>
        <cfinput type="submit" name="fld_submitEdit" id="fld_submitEdit" value="Add" />
		<a href="/testMovieApp/index.cfm"><button type="button">Cancel</button></a>      
    </fieldset>
</cfform>






</body>
</html>