component persistent="true" table ="users" output="false" {
	property name="user_id" fieldtype="id" column="user_id" generator="identity";
	property name="name" length="100" notnull="true" type="string";
	property name="email" length="200" notnull="true" type="string";
	property name="password" length="500" notnull="true" type="string";
	property name="created_at"  notnull="true" ormtype="timestamp";
	
	public any function init(){
		if (IsNull(variables.created_at)){
			variables.created_at = now();
		}
		return this;
	}

}
