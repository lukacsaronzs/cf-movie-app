component persistent="true" table="movies" entityname="Movie" output="false" {
	property name="movie_id" fieldtype="id" type="numeric" generator="native" notnull="true";
	property name="title" length="60" notnull="true" type="string";
	property name="description" length="500" notnull="false" type="string";
	property name="price" nullable="false" ormtype="big_decimal"  default="0.0";
	property name="release_year" nullable="true" ormtype="int";
	property name="created_at" nullable="false" ormtype="timestamp";
	property name="updated_at" nullable="true" ormtype="timestamp" notnull="false"; 
	property name="created_by" fieldtype="many-to-one" cfc="user" fkcolumn="created_by";
	property name="updated_by" fieldtype="many-to-one" cfc="user" fkcolumn="updated_by" notnull="false";
	
	public any function init(){
		if (IsNull(variables.created_at)){
			variables.created_at = now();
		}
		return this;
	}
	
	public any function GETCREATED_BY ( ){
		if (this.hascreated_by() and !IsNull(variables.created_by)){
			return variables.created_by.getname()
			
		}
		else{
			return 'a';
		}
	}
	
	public any function GETUPDATED_BY ( ){
		if (this.hasupdated_by() and not IsNull(variables.updated_by)){
			return variables.updated_by.getname()
			
		}
		else{
			return '';
		}
	}

	
	



}
