component output = "false" {
	this.name = "testMovieApp";
	this.ormEnabled = true;
	this.dataSource = "testMovie";
	this.ormSettings = {};
	this.ormSettings.cfclocation = "Model";
	this.ormSettings.dbcreate = "update";

	this.sessionManagement = true;
	
	//Application start method

	function onApplicationStart() {
	application.userService = createObject("component", 'testMovieApp.Components.userService' );
	application.movieService = createobject("component",'testMovieApp.Components.movieService' );
	application.authenticationService = createobject("component",'testMovieApp.Components.authenticationService' );
	ormReload();
	return true;
	
	}

//onRequestStart() method

	boolean function onRequestStart() {
	
	//Handle special URL parameters
		if ( isDefined('url.restartApp') ) {
			this.onApplicationStart();
			}	
		return true;
	}
}
