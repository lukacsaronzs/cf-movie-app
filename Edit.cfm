<cfinclude template="./Includes/accesscontrol.cfm" /> 



<cfif structKeyExists(form,'fld_submitEdit')>
		<!---Proceed to edit procedure--->
		<cfset aErrorMEssages = application.movieService.validateMovie(fld_movieTitle,fld_movieDescription,fld_movieReleaseYear,fld_moviePrice)/>
		<cfif arraylen(aErrorMEssages) EQ 0>
			<cfset execute = application.movieService.updateMovie(fld_movieId,fld_movieTitle,fld_movieDescription,fld_movieReleaseYear,fld_moviePrice,LSParseNumber(fld_userId))>
			<cfif execute>
				<cflocation url="/testMovieApp/index.cfm" />
			</cfif>
		</cfif>
</cfif>

<cfinclude template="./Includes/head.cfm" /> 


<cfset movie = application.movieService.getMovieByID(url.movieId)>

<cfinclude template="./Includes/loginForm.cfm" /> 

<cfif structkeyExists(url,'movieId')>
	<cfform id="frmConnexion" preservedata="true">
	<fieldset>
    <legend>Edit movie</legend>
					<cfif isDefined('aErrorMessages') AND NOT ArrayIsEmpty(aErrorMessages)>	
					<cfoutput >
						<cfloop array = "#aErrorMessages#" index = "message">
							<p class="errorMessage">#message#</p>
						</cfloop>
					</cfoutput>
				</cfif>
		<dl>
			<dd><cfinput type="hidden" name="fld_movieId" id="fld_movieId" required="true" value = #movie.getmovie_id()#/></dd>
			<dd><cfinput type="hidden" name="fld_userId" id="fld_userId" required="true" value = #session.loggedInUser.getuser_id()#/></dd>

        	<dt><label for="fld_movieTitle">Title</label></dt>
            <dd><cfinput type="text" name="fld_movieTitle" id="fld_movieTitle" required="true" value = #movie.gettitle()#/></dd>
            
            <dt><label for="fld_movieDescription">Description</label></dt>
            <dd><cfinput type="text" name="fld_movieDescription" id="fld_movieDescription" required="true"  value = #movie.getdescription()#/></dd>
            
            <dt><label for="fld_movieReleaseYear">Release year</label></dt>
            <dd><cfinput type="text" name="fld_movieReleaseYear" id="fld_movieReleaseYear" required="true"  value = #movie.getrelease_year()#/></dd>
            
            <dt><label for="fld_moviePrice">Price</label></dt>
            <dd><cfinput type="text" name="fld_moviePrice" id="fld_moviePrice" required="true"  value = #movie.getprice()#/></dd>
            
    		
        </dl>
        <cfinput type="submit" name="fld_submitEdit" id="fld_submitEdit" value="Edit" />
		<a href="/testMovieApp/index.cfm"><button type="button">Cancel</button></a>      
    </fieldset>
</cfform>

<cfelse>
	<cflocation url="/testMovieApp/index.cfm?noaccess" />

</cfif>

</body>
</html>