<!---Handling logout--->
<cfif structKeyExists(url,'logout')>
	<cfset application.authenticationService.doLogout() />
</cfif>


	
<!---Form processing starts here--->
<cfif structKeyExists(form,'fld_submitLogin')>
		<!---Proceed to login procedure--->
		<cfset userLoggedIn = application.authenticationService.doLogin(fld_userEmail,fld_userPassword)/>
</cfif>
<!---Form processing ends here--->


<cfform id="frmConnexion" preservedata="true">
	<fieldset>
    <legend>Login</legend>

    <cfif structKeyExists(variables,'userLoggedIn') AND userLoggedIn EQ false>
    	<p class=errorMessage>Invalid credentials.</p>
    </cfif>
    <cfif structKeyExists(url,'noaccess')>
    	<p class=errorMessage>Log in first.</p>
    </cfif>
    <cfif structKeyExists(session,'loggedInUser')>
    <!--Welcome message---->
    	<p><cfoutput >Welcome #session.loggedInUser.getname()#  </cfoutput></p>
         <p><a href="/testMovieApp/index.cfm?logout">Logout</a></p>
         

    	
    <cfelse>
    <!--Login form---->
		<dl>
        	<dt><label for="fld_userEmail">E-mail address</label></dt>
            <dd><cfinput type="text" name="fld_userEmail" id="fld_userEmail" required="true" validate="email" validateAt="onSubmit" message="Please enter a valid e-mail address!" /></dd>
    		<dt><label for="fld_userPassword">Password</label></dt>
            <dd><cfinput type="password" name="fld_userPassword" id="fld_userPassword" required="true"  validateAt="onSubmit" message="Please provide a password!" /></dd>
        </dl>
        <cfinput type="submit" name="fld_submitLogin" id="fld_submitLogin" value="Login" />
		<a href="/testMovieApp/register.cfm"><button type="button">Register</button></a>      
		</cfif>
    </fieldset>
</cfform>
