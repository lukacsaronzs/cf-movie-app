<cfinclude template="./Includes/head.cfm" /> 
<cfinclude template="./Includes/loginForm.cfm" /> 

		<h1>Welcome to the Test Movie App</h1>
		<cfif structKeyExists(session,'loggedInUser')>
			<p>Hurray! You logged in.</p>
			
			<table id="movie_table" class="display">
				 <thead>
		            <tr>
		            	<th>Movie ID</th>
		                <th>Title</th>
		                <th>Description</th>
		                <th>Release year</th>
		                <th>Price</th>
		                <th>Created By</th>
		                <th>Created At</th>
		                <th>Updated By</th>
		                <th>Updated At</th>
		                <th>Actions</th>
		                
		            </tr> 
		         </thead>
		        
		       
			</table>
			<input type="button" class="add" value="Add"/>
	

<cfelse>
		<p>Please log in first to see all the content.</p>

</cfif>

</body>
</html>


	<script >

	var table = $('#movie_table').DataTable( {
	searching: false,
	processing: true,
    serverSide: true,
   	ajax : {
    	url: "http://127.0.0.1:8600/rest/api/all.json",
    	type: "POST",
    	datatype: "json"
    },
    columns: [
   		{ 
   			data: 'movie_id' ,
   			"visible" : true},
        { data: 'title' },
        { data: 'description' },
        { data: 'release_year' },
        { data: 'price' },
        {data: 'created_by'},
        {data: 'created_at'},
        {data: 'updated_by'},
        {data: 'updated_at'},
        {
            defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'

        },
	    	],
    	
} );
  
$('#movie_table tbody').on('click', '.edit', function () {
  var row = $(this).closest('tr');
  
  var data = table.row( row ).data();
  window.location.href = "Edit.cfm?movieId=" + data.movie_id;
});
  
  
$('#movie_table tbody').on('click', '.delete', function () {
  var row = $(this).closest('tr');
  
  var data = table.row( row ).data();
  console.log(data);
  window.location.href = "Delete.cfm?movieId=" + data.movie_id;
});

$('.add').on('click', function () {
  
  window.location.href = "Create.cfm";
});
	</script>


